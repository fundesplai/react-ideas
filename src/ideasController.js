
const ideasController = {};


 ideasController.leer = () => {
        const json = localStorage.getItem('ideasData');
        if (!json) return [];
        try {
            return JSON.parse(json);
        } catch {n
            return [];
        }
    }

ideasController.guardar = (data) => {
        const json = JSON.stringify(data);
        localStorage.setItem('ideasData', json);
    }

export default ideasController;