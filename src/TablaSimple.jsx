
import {Table} from 'react-bootstrap';


function TablaSimple(props) {
    const filas = props.datos.map((idea, idx) => <tr key={idx}><td>{idea}</td><td><div className="borrar" onClick={()=>props.borrar(idea)} >X</div></td></tr>)
    return (
      <>
        <Table>
          <tbody>
            {filas}
          </tbody>
        </Table>
      </>
    )
  }

  export default TablaSimple;