import './App.css'
import { useState } from 'react';

import { Container, Form, Row, Col, Table, Button } from 'react-bootstrap';

import TablaSimple from './TablaSimple';


function cargar() {
  const datosguardados = localStorage.getItem('ideasData');
  if (!datosguardados) return [];

  try {
    return JSON.parse(datosguardados);
  } catch {
    return [];
  }
}


function guardar(listaDeCosas){
  const json = JSON.stringify(listaDeCosas);
  localStorage.setItem('ideasData', json);
}

function App() {

  const [ideas, setIdeas] = useState(cargar());
  const [nueva, setNueva] = useState("");
  const [uid, setUid] = useState("");

  function nuevaIdea() {
    const nuevasIdeas = [nueva, ...ideas];
    setIdeas(nuevasIdeas);
    guardar(nuevasIdeas);
    setNueva("");
  }

  function borrar(x){
    console.log("borrando idea "+x);
    const nuevasIdeas = ideas.filter(e => e!==x);
    setIdeas(nuevasIdeas);
    guardar(nuevasIdeas);
  }

  return (
    <Container>

      <br />
      <h2>Idees...</h2>
      <br />
      <Row>
        <Col>
          <Form.Group className="mb-3" controlId="idea">
            <Form.Label>Nueva idea</Form.Label>
            <Form.Control value={nueva} onInput={(e) => setNueva(e.target.value)} type="text" placeholder="Entra idea" />
          </Form.Group>
          <Button onClick={nuevaIdea} >Entrar</Button>
        </Col>
        <Col>
          <TablaSimple datos={ideas} borrar={borrar}/>
        </Col>

      </Row>



    </Container>

  )
}

export default App
